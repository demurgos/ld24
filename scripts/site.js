$(document).ready(function () {
	$("#footerhandler").click(function(){

		var content=$("#footercontent");

		if(content.is(':visible')){

			$(this).text('OPEN');
			$("#footercontent").slideUp();

		}else{
			$(this).text('CLOSE');
			$("#footercontent").slideDown();

		}

	});
});



hf=new function(){

	this.list=['meteor', 'fire', 'wheat', 'h5n1', 'dog', 'excalibur', 'beer','killer', 'noob', 'pokeball' ];

	this.done=getSave('hf')?getSave('hf'):[];

	this.req={
		'meteor': 50,
		'h5n1': 25,
		'dog': 100,
		'killer': 400,
		'noob': 50,
	}
	,
	this.unlock=function(hf){

		if($.inArray(hf,this.done)<0){
			this.done.push(hf);
			setSave('hf',this.done);
		};

		var li=$('#hf_'+hf);

		li.addClass('done');


		var ul=$('#achievements');

		li.scrollTop();

	};

	this.lock=function(hf){

		if((i=$.inArray(hf,this.done))>=0){
			this.done.splice(i,1);
			setSave('hf',this.done);
		};

		var li=$('#hf_'+hf);

		li.removeClass('done');


	};

	for(var i=0,c=this.done.length;i<c;i++){

		this.unlock(this.done[i]);

	}

	this.got=function(hf){
		return $.inArray(hf,this.done)>=0;
	};

	this.hf2car=function(){

		var ans={'velX':4,'velY':13};

		if(this.got('fire')){
			ans.velX+=10;
		}
		if(this.got('wheat')){
			ans.velY+=3;}

		if(this.got('excalibur')){
			ans.velX += 5;
		}
		if(this.got('beer')){
			ans.velY+=2;}
		if(this.got('pokeball')){
			ans.pokeball=true;}

		return ans;

	};

};
/*
$('#achievements li')
	.each(function(i,e){

		if(i<hf.list.length){

			$(e).click(function(){

				if($(e).is('.done')){
					hf.lock(hf.list[i]);
				}else{
					hf.unlock(hf.list[i]);
				}


			});

		}

	});
*/


$('#erasedatas').click(function(){

	if(confirm('Are you sure that you want to erase all datas ?')){
		for(var i in localStorage){

			localStorage.removeItem(i);

		}
		alert('Done, the page should be refreshed');
		document.location.href=document.location.href+'';
	}
});

async function loadAudio(uri) {
	return new Promise((resolve, reject) => {
		const audio = new Audio(uri);
		let complete = false;
		audio.addEventListener("canplaythrough", (_ev) => {
			complete = true;
			resolve(audio);
		});
		audio.addEventListener("error", (ev) => {
			complete = true;
			reject(ev);
		})
	})
}

async function prepareAudio() {
	const tracks = new Map([
		["main", "data/audio/main.mp3"],
		["prehistory", "data/audio/prehistory.mp3"],
		["middle_age", "data/audio/middle_age.mp3"],
		["modern", "data/audio/modern.mp3"],
	]);
	const promises = [];
	for (const [key, uri] of tracks) {
		promises.push(loadAudio(uri).then((audio) => {
			audio.loop = true;
			Reflect.set(game.mp3, key, audio);
		}))
	}
	await Promise.all(promises);
}

const gameDiv = document.getElementById("game");

gameDiv.addEventListener("click", async () => {
	await prepareAudio();
	await onAudioReady();
}, {once: true});

async function onAudioReady() {
	var lvl=getSave('MapArea');

	switch(lvl){

		case 'prehistory_1' :
		case 'prehistory_2' :
		case 'prehistory_3' :
			game.mp3.prehistory.play();
			game.curMusic = game.mp3.prehistory;
		break;


		case 'middle_age_1' :
		case 'middle_age_1_1' :
		case 'middle_age_2' :
		case 'middle_age_3' :
			game.mp3.middle_age.play();
			game.curMusic = game.mp3.middle_age;

		break;

		case 'modern_1' :
		case 'modern_2' :
		case 'modern_3' :
			game.mp3.modern.play();
			game.curMusic = game.mp3.modern;

		break;

		default:
			game.mp3.main.play();
			game.curMusic = game.mp3.main;
	}


	if(game.mute){
		game.curMusic.volume = 0;
		$('#sound').text('Play Music');
	}
}

$('#sound').click(function(){

	var btn=$(this);

	if(btn.text()=='Mute'){

		game.mute=true;
		setSave('mute',true);
		game.curMusic.volume = 0;

		//alert('Mute')

		btn.text('Play Music');

	}else{

		game.mute=false;
		setSave('mute',false);
		game.curMusic.volume = 1;

		btn.text('Mute');

	}





})


dial=new function(){

	//this.list=['meteor', 'fire', 'wheat', 'h5n1', 'dog', 'excalibur', 'beer','killer', 'noob', 'pokeball' ];

	this.done=[];

	this.unlock=function(id,txt){

		if(!this.got(id)){
			this.done.push(id);
			setSave('dial',this.done);
			log(txt);
		};


	};

	this.lock=function(id){

		/*if(this.got(id)){
			this.done.splice(i,1);
			setSave('dial',this.done);
		};*/
	};

	/*for(var i=0,c=this.done.length;i<c;i++){

		this.unlock(this.done[i]);

	}*/

	this.got=function(hf){
		return $.inArray(hf,this.done)>=0;
	};

	/*this.hf2car=function(){

		var ans={'velX':4,'velY':13};

		if(this.got('fire')){
			ans.velX+=10;
		}
		if(this.got('wheat')){
			ans.velY+=3;}

		if(this.got('excalibur')){
			ans.velX += 2;
		}
		if(this.got('beer')){
			ans.velY+=2;}

		return ans;

	};*/

};

