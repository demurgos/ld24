//window['Player'];


var Player = me.ObjectEntity.extend({
	init: function(x, y, settings) {
		this.parent(x, y, settings);
		this.initX = x;
		this.initY = y;
		this.settings = settings;
		this.name = "player";
		this.currentAnim = "stand";
		me.debug.renderHitBox = false; //Debug
		this.updateColRect(25, 15, 33, 31); //ORIGINAL VALUES 5/46/10/45
		
		var car=hf.hf2car();
		
		this.setMaxVelocity (car.velX,car.velY); // ORIGINAL VALUES 4 / 13
		this.animationspeed = 5;
		this.collidable = true;
		me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
		this.addAnimation("stand", [0]);
		this.addAnimation("jump", [3]);
		this.addAnimation("breath", [0, 1, 2, 3]);
		this.addAnimation("walk", [4, 5, 6, 7, 8]);
		this.setCurrentAnimation("stand");
		this.alive = true; //Tu es vivant
		this.canBreakTile = true; //Peut casser les tiles
		game.player=this;
	},
	
	doChangeState: function(state) {
		this.setCurrentAnimation(state);
		this.currentAnim = state;
	},
	
	walk: function() {
		this.currentAnim = "walk";
		this.setCurrentAnimation("walk");
		return true;
	},
	
	die: function() {
		updateSave('Death', 1);
		if(getSave('Death') >= hf.req['noob']) { hf.unlock('noob'); }
		this.flicker(42); //Ne fait plus effet :-(
		me.levelDirector.loadLevel(getSave('MapArea'));//recharge le niveau pour faire repop les ennemis
		this.respawn();
		updateScore(-100);
		me.game.HUD.setItemValue("Death", getSave('Death'));
	}, 
	
	respawn:function(){
		 
		 this.pos.x=this.initX;
		 this.pos.y=this.initY;
	
	},
	
	update : function () {
		if (me.input.isKeyPressed('left')) {			
			this.walk();
			if(this.jumping){this.setCurrentAnimation("jump");}
			this.vel.x = -5;
			this.flipX(false);
		} else if (me.input.isKeyPressed('right')) {
			this.walk();
			if(this.jumping){this.setCurrentAnimation("jump");}
			this.vel.x = 5;
			this.flipX(true);
		} else {
			this.vel.x = 0;
			this.setCurrentAnimation("stand");
		}
		if (me.input.isKeyPressed('jump')) {
				this.doJump();
				this.setCurrentAnimation("jump");
		}
		this.updateMovement();
		var res = me.game.collide(this);
		if (res) {
	
				if (res.obj.type == me.game.ENEMY_OBJECT) { //Collision avec ennemy
					 // check if we jumped on it
					if ((res.obj.vel.y < 5) && ! this.jumping) {
						// bounce (force jump)
						this.falling = false;
						this.vel.y = -this.maxVel.y * me.timer.tick;
						// set the jumping flag
						this.jumping = true;
						this.doJump();
		 
					} else {
						// Death
						this.die();
					}
				}
				else
				{
					if(res.obj.name == "crate") {
						if (this.vel.x != 0 || this.vel.y != 0) {
							this.vel.x = 0;
							console.log("Collision : Plr PosX : " + this.pos.x + " / Obj PosX : "+ res.obj.pos.x + " / DeltaX : "+res.x);
							console.log("            Plr PosY : " + Math.round(this.pos.y) + " / Obj PosY : "+ res.obj.pos.y + " / DeltaY : "+Math.round(res.y));
							if (res.y > 0) {
								console.log("Obj is under.");
								//this.vel.y = -1;
								this.pos.y -= 30;
							}
							else if (res.x > 0) {
								console.log("Obj is on the right.");
								this.pos.x -= 3;
								if (me.input.isKeyPressed('use')) {
									res.obj.pos.x += 4;
								}
							} else 	if (res.x < 0) {
								console.log("Obj is on the left.");
								this.pos.x += 3;
								if (me.input.isKeyPressed('use')) {
									res.obj.pos.x -= 4;
								}
							}
						}
					}
					this.updateMovement();
				}
			
		}
		this.parent(this);
		return true;
	}
});



/* --------------------------
an enemy Entity
------------------------ */
var RedDinoEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        // define this here instead of tiled
        settings.image = "RedDino";
        settings.spritewidth = 64;
 
        // call the parent constructor
        this.parent(x, y, settings);
 
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite
 
        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = true;
 
        // walking & jumping speed
        this.setVelocity(3, 6);
 
        // make it collidable
        this.collidable = true;
		this.updateColRect(19, 25, 31, 33);
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;
 
    },
 
    // call by the engine when colliding with another object
    // obj parameter corresponds to the other object (typically the player) touching this one
    onCollision: function(res, obj) {
 
        // res.y >0 means touched by something on the bottom
        // which mean at top position for this one
        if (this.alive && (obj.vel.y >= 5) && obj.falling) {
            this.flicker(45);
			updateScore(100);
			 // Death
			me.game.remove(this);
				//Récompense
				updateSave('KillDinos', 1);
				if(getSave('KillDinos') >= hf.req['meteor']) { hf.unlock('meteor'); }
				updateSave('Killer', 1);
				if(getSave('Killer') >= hf.req['killer']) { hf.unlock('killer'); }
			
        }
    },
 
    // manage the enemy movement
    update: function() {
        // do nothing if not visible
        if (!this.visible)
            return false;
 
        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;
                 
        } else {
            this.vel.x = 0;
        }
         
        // check and update movement
        this.updateMovement();
         
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update objet animation
            this.parent(this);
            return true;
        }
        return false;
    }
});

var BlueDinoEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        // define this here instead of tiled
        settings.image = "BlueDino";
        settings.spritewidth = 64;
 
        // call the parent constructor
        this.parent(x, y, settings);
 
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite
 
        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = true;
 
        // walking & jumping speed
        this.setVelocity(4, 6);
 
        // make it collidable
        this.collidable = true;
		this.updateColRect(19, 25, 31, 33);
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;
 
    },
 
    // call by the engine when colliding with another object
    // obj parameter corresponds to the other object (typically the player) touching this one
    onCollision: function(res, obj) {
 
        // res.y >0 means touched by something on the bottom
        // which mean at top position for this one
        if (this.alive && (obj.vel.y >= 5) && obj.falling) {
            this.flicker(45);
			updateScore(200);
			 // Death
			me.game.remove(this);
				//Récompense
				updateSave('KillDinos', 1);
				if(getSave('KillDinos') >= hf.req['meteor']) { hf.unlock('meteor'); }
				updateSave('Killer', 1);
				if(getSave('Killer') >= hf.req['killer']) { hf.unlock('killer'); }
        }
    },
 
    // manage the enemy movement
    update: function() {
        // do nothing if not visible
        if (!this.visible)
            return false;
 
        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;
                 
        } else {
            this.vel.x = 0;
        }
         
        // check and update movement
        this.updateMovement();
         
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update objet animation
            this.parent(this);
            return true;
        }
        return false;
    }
});

var PteroEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        // define this here instead of tiled
        settings.image = "Ptero";
        settings.spritewidth = 64;
 
        // call the parent constructor
        this.parent(x, y, settings);
 
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite
 
        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = true;
 
        // walking & jumping speed
        this.setVelocity(1.5, 6);
		this.gravity = 0;
        // make it collidable
        this.collidable = true;
		this.updateColRect(11/*d_x*/, 37/*w*/, 31/*d_y*/, 20/*h*/);
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;
 
    },
 
    // call by the engine when colliding with another object
    // obj parameter corresponds to the other object (typically the player) touching this one
    onCollision: function(res, obj) {
 
        // res.y >0 means touched by something on the bottom
        // which mean at top position for this one
        if (this.alive && (obj.vel.y >= 5) && obj.falling) {
            this.flicker(45);
			updateScore(300);
			 // Death
			me.game.remove(this);
				//Récompense
				updateSave('KillDinos', 1);
				if(getSave('KillDinos') >= hf.req['meteor']) { hf.unlock('meteor'); }
				updateSave('Killer', 1);
				if(getSave('Killer') >= hf.req['killer']) { hf.unlock('killer'); }
			
        }
    },
 
    // manage the enemy movement
    update: function() {
        // do nothing if not visible
        if (!this.visible)
            return false;
 
        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;
                 
        } else {
            this.vel.x = 0;
        }
         
        // check and update movement
        this.updateMovement();
         
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update objet animation
            this.parent(this);
            return true;
        }
        return false;
    }
});



var ChickenEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        // define this here instead of tiled
        settings.image = "chicken";
        settings.spritewidth = 64;
 
        // call the parent constructor
        this.parent(x, y, settings);
 
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite
 
        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = true;
 
        // walking & jumping speed
        this.setVelocity(3, 6);
 
        // make it collidable
        this.collidable = true;
		this.updateColRect(19, 29, 31, 33);
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;
 
    },
 
    // call by the engine when colliding with another object
    // obj parameter corresponds to the other object (typically the player) touching this one
    onCollision: function(res, obj) {
 
        // res.y >0 means touched by something on the bottom
        // which mean at top position for this one
        if (this.alive && (obj.vel.y >= 5) && obj.falling) {
            this.flicker(45);
			updateScore(200);
			 // Death
			me.game.remove(this);
				//Récompense
				updateSave('KillChicken', 1);
				if(getSave('KillChicken') >= hf.req['h5n1']) { hf.unlock('h5n1'); }
				updateSave('Killer', 1);
				if(getSave('Killer') >= hf.req['killer']) { hf.unlock('killer'); }
        }
    },
 
    // manage the enemy movement
    update: function() {
        // do nothing if not visible
        if (!this.visible)
            return false;
 
        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;
                 
        } else {
            this.vel.x = 0;
        }
         
        // check and update movement
        this.updateMovement();
         
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update objet animation
            this.parent(this);
            return true;
        }
        return false;
    }
});

var DogEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        // define this here instead of tiled
        settings.image = "dog";
        settings.spritewidth = 64;
 
        // call the parent constructor
        this.parent(x, y, settings);
 
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite
 
        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = true;
 
        // walking & jumping speed
        this.setVelocity(3, 6);
 
        // make it collidable
        this.collidable = true;
		this.updateColRect(12, 46, 31, 33);
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;
 
    },
 
    // call by the engine when colliding with another object
    // obj parameter corresponds to the other object (typically the player) touching this one
    onCollision: function(res, obj) {
 
        // res.y >0 means touched by something on the bottom
        // which mean at top position for this one
        if (this.alive && (obj.vel.y >= 5) && obj.falling) {
            this.flicker(45);
			updateScore(300);
			 // Death
			me.game.remove(this);
				//Récompense
				updateSave('KillDog', 1);
				if(getSave('KillDog') >= hf.req['dog']) { hf.unlock('dog'); }
				updateSave('Killer', 1);
				if(getSave('Killer') >= hf.req['killer']) { hf.unlock('killer'); }
        }
    },
 
    // manage the enemy movement
    update: function() {
        // do nothing if not visible
        if (!this.visible)
            return false;
 
        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;
                 
        } else {
            this.vel.x = 0;
        }
         
        // check and update movement
        this.updateMovement();
         
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update objet animation
            this.parent(this);
            return true;
        }
        return false;
    }
});

var ChopperEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        // define this here instead of tiled
        settings.image = "Chopper";
        settings.spritewidth = 64;
 
        // call the parent constructor
        this.parent(x, y, settings);
 
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite
 
        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = true;
 
        // walking & jumping speed
        this.setVelocity(1.5, 6);
		this.gravity = 0;
        // make it collidable
        this.collidable = true;
		this.updateColRect(11/*d_x*/, 45/*w*/, 31/*d_y*/, 27/*h*/);
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;
 
    },
 
    // call by the engine when colliding with another object
    // obj parameter corresponds to the other object (typically the player) touching this one
    onCollision: function(res, obj) {
 
        // res.y >0 means touched by something on the bottom
        // which mean at top position for this one
        if (this.alive && (obj.vel.y >= 5) && obj.falling) {
            this.flicker(45);
			updateScore(300);
			 // Death
			me.game.remove(this);
				//Récompense
				updateSave('Killer', 1);
				if(getSave('Killer') >= hf.req['killer']) { hf.unlock('killer'); }
			
        }
    },
 
    // manage the enemy movement
    update: function() {
        // do nothing if not visible
        if (!this.visible)
            return false;
 
        if (this.alive) {
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.walkLeft = false;
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.walkLeft = true;
            }
            // make it walk
            this.flipX(this.walkLeft);
            this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;
                 
        } else {
            this.vel.x = 0;
        }
         
        // check and update movement
        this.updateMovement();
         
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update objet animation
            this.parent(this);
            return true;
        }
        return false;
    }
});

//Entités invisbles tueurs :D
var InvisibleEntity = me.InvisibleEntity.extend({
	init: function(x, y, settings) {
		this.parent(x, y, settings);
		this.initX = x;
		this.initY = y;
		this.settings = settings;

	},
	
	onCollision: function(res, obj)
	{
		obj.die();
	}
});

var EndEntity = me.InvisibleEntity.extend({
	init: function(x, y, settings) {
		this.parent(x, y, settings);
		this.initX = x;
		this.initY = y;
		this.settings = settings;

	},
	
	onCollision: function(res, obj)
	{
		alert('SORRY ! WE HAVEN\'T ENOUGH TIME FOR THE END : YOU ARE DEAD, KILLED BY THE APOCALYSPE ! IT\'S THE END OF THE WORLD !!! YOU CAN RETRY TO FIND ALTERNATE ENDINGS/HISTORY');
		if(hf.got('pokeball'))
		{
			//Bravo !
			  me.levelDirector.loadLevel('secret_end'); 
		}
		else
		{
			window.href = 'end.html';
		}
	},
});


// Warp Entity
var Warper = me.InvisibleEntity.extend({
	init: function(x, y, settings) {
		this.parent(x, y, settings);
		this.initX = x;
		this.initY = y;
		this.settings = settings;

	},
	
	onCollision: function(res, obj)
	{
		if (me.input.isKeyPressed('use')) {
			me.levelDirector.loadLevel(this.settings.to);
			setSave('MapArea', this.settings.to);
		}
	}
});

var DoDialog = me.InvisibleEntity.extend({
	init: function(x, y, settings) {
		this.parent(x, y, settings);
		this.initX = x;
		this.initY = y;
		this.settings = settings;

	},
	
	onCollision: function(res, obj)
	{
		dial.unlock(this.settings.id,this.settings.text);
		me.game.remove(this);
		this.collidable = false;
		dial.lock(this.settings.id);
	}
});


/*----------------
 LES OBJETS
------------------------ */
var ItemPokeball = me.CollectableEntity.extend({
   
    init: function(x, y, settings) {
        // call the parent constructor
        this.parent(x, y, settings);
    },
 
    onCollision: function() {
        // do something when collected
		me.game.remove(this);
		updateScore(10000);
		hf.unlock('pokeball');
    }
 
});

/*--------------
a score HUD Item
--------------------- */
 
var ScoreObject = me.HUD_Item.extend({
    init: function(x, y) {
        // call the parent constructor
        this.parent(x, y);
        // create a font
        this.font = new me.BitmapFont("32x32_font", 32);
    },
 
    /* -----
 
    draw our score
 
    ------ */
    draw: function(context, x, y) {
        this.font.draw(context, this.value, this.pos.x + x, this.pos.y + y);
    }
 
});

var TextDieObject = me.HUD_Item.extend({
    init: function(x, y) {
        // call the parent constructor
        this.parent(x, y);
        // create a font
        this.font = new me.BitmapFont("32x32_font", 32);
    },
 
    draw: function(context, x, y) {
        this.font.draw(context, this.value, this.pos.x + x, this.pos.y + y);
    }
 
});

var DieObject = me.HUD_Item.extend({
    init: function(x, y) {
        // call the parent constructor
        this.parent(x, y);
        // create a font
        this.font = new me.BitmapFont("32x32_font", 32);
    },
 
    draw: function(context, x, y) {
        this.font.draw(context, this.value, this.pos.x + x, this.pos.y + y);
    }
 
});




 