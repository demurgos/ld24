/*
	Liste des Saves :
	MapArea (string) : la map en cours
	Score (int) : Score du joueur
    Inventory (array) : Invetaire TODO
	Achievements (array) : Récompenses TODO
*/
(function(window){

	var game=(function(){
	
	

		var game={
		
			'mp3':{},
		
			'epoqs':[
			
				{
					'name':'Prehistory',
					'char':'caveman',
					'lvl':'Area01'
				},{
					'name':'MiddleAge',
					'char':'peasant',
					'lvl':'Area01'
				},{
					'name':'Today',
					'char':'modernman',
					'lvl':'Area01'
				},
			
			]
		
		};
		
		game.curEpoq=game.epoqs[0];
		
		game.curMusic=false;
		
		game.mute=getSave('mute');
		
		//if(game.mute){this.curMusic.mute()};
		
		game.levelChange=function(lvl){
		
			this.curMusic.stop();
		
			switch(lvl){
			
				case 'prehistory_1' :
				case 'prehistory_2' :
				case 'prehistory_3' :

					this.curMusic=this.mp3.prehistory;
					this.mp3.prehistory.play();
					
				break;	
						
					
				case 'middle_age_1' :
				case 'middle_age_1_1' :
				case 'middle_age_2' :
				case 'middle_age_3' :
				
					this.curMusic=this.mp3.middle_age;
					this.mp3.middle_age.play();
					
				break;
					
				case 'modern_1' :
				case 'modern_1_1' :
				case 'end' :
				case 'secret_end' :
				case 'city_shop_1' :
				
					this.curMusic=this.mp3.modern;
					this.mp3.modern.play();
				
				break;
				
				default:
				
					this.curMusic=this.mp3.main;
					this.mp3.main.play();
			
			}


			if(game.mute){this.curMusic.mute();}
		
			if(lvl == "prehistory_2")
			{
				hf.unlock('fire');
				updateScore(1000);
			}
			else if(lvl == "prehistory_3")
			{
				hf.unlock('wheat');
				updateScore(2000);
			}
			else if(lvl == "middle_age_2")
			{
				hf.unlock('excalibur');
				updateScore(1000);
			}
			else if(lvl == "middle_age_3")
			{
				hf.unlock('beer');
				updateScore(2000);
			}
			else if(lvl == "modern_2")
			{
				updateScore(1000);
			}
			else if(lvl == "end")
			{
				updateScore(2000);
			}
			else if(lvl == "secret_end")
			{
				//Fin secrète
				updateScore(10000);
			}
			else
			{
				//Nada
			}
		
		};
		
		return game;
	
	
	})();


	window['game']=game;

})(window);

var Achievements = new Array();

//Init des Saves
if(typeof(getSave('Logs')) == "string") {
	updateSave('Logs', '<br />');

} else {
	//Pas de save existante
	setSave('Logs', '<br />');
}
if(typeof(getSave('MapArea')) == "string") {
	updateSave('Logs', 'Load Save MapArea : '+getSave('MapArea')+ '<br />');
} else {
	//Pas de save existante
	setSave('MapArea', 'prehistory_1');
	updateSave('Logs', 'Add Save MapArea : prehistory_1 <br />');
}

if(typeof(getSave('Score')) == "number") {
	updateSave('Logs', 'Load Save Score : '+getSave('Score')+ '<br />');
} else {
	//Pas de save existante
	setSave('Score', 0);
	updateSave('Logs', 'Add Save Score(0) <br />');
}

if(typeof(getSave('Death')) == "number") {
	updateSave('Logs', 'Load Save Death : '+getSave('Death')+ '<br />');
} else {
	//Pas de save existante
	setSave('Death', 0);
	updateSave('Logs', 'Add Save Death(0) <br />');
}

if(typeof(getSave('Killer')) == "number") {
	updateSave('Logs', 'Load Save Killer : '+getSave('Killer')+ '<br />');
} else {
	//Pas de save existante
	setSave('Killer', 0);
	updateSave('Logs', 'Add Save Killer(0) <br />');
}

if(typeof(getSave('KillDinos')) == "number") {
	updateSave('Logs', 'Load Save KillDinos : '+getSave('KillDinos')+ '<br />');
} else {
	//Pas de save existante
	setSave('KillDinos', 0);
	updateSave('Logs', 'Add Save KillDinos(0) <br />');
}

if(typeof(getSave('KillChicken')) == "number") {
	updateSave('Logs', 'Load Save KillChicken : '+getSave('KillChicken')+ '<br />');
} else {
	//Pas de save existante
	setSave('KillChicken', 0);
	updateSave('Logs', 'Add Save KillChicken(0) <br />');
}

if(typeof(getSave('KillDog')) == "number") {
	updateSave('Logs', 'Load Save KillDog : '+getSave('KillDog')+ '<br />');
} else {
	//Pas de save existante
	setSave('KillDog', 0);
	updateSave('Logs', 'Add Save KillDog(0) <br />');
}


	
function play(){



}

/* Init test */
//game resources
var g_resources = [{
    name: "dirt",
    type: "image",
    src: "data/tilesets/dirt.png"
}, {
	name: "items",
	type: "image",
	src: "data/tilesets/items.png"
},{
	name: "diverse",
	type: "image",
	src: "data/tilesets/diverse.png"
}, {
	name: "field",
	type: "image",
	src: "data/tilesets/field.png"
}, {
	name: "fire",
	type: "image",
	src: "data/tilesets/fire.png"
}, {
	name: "GoldBricks",
	type: "image",
	src: "data/tilesets/GoldBricks.png"
},
{
	name: "misc_scenery",
	type: "image",
	src: "data/tilesets/misc_scenery.png"
},{
	name: "platform",
	type: "image",
	src: "data/tilesets/platform.png"
},
 {
	name: "grass",
	type: "image",
	src: "data/tilesets/grass.png"
}, {
	name: "grassy-ground",
	type: "image",
	src: "data/tilesets/grassy-ground.png"
}, {
	name: "tileset",
	type: "image",
	src: "data/tilesets/tileset.gif"
}, {
	name: "industrial",
	type: "image",
	src: "data/tilesets/industrial.png"
}, {
	name: "metal",
	type: "image",
	src: "data/tilesets/metal.png"
}, {
	name: "furniture",
	type: "image",
	src: "data/tilesets/furniture.png"
}, {
	name: "metatiles16x16",
	type: "image",
	src: "data/tilesets/metatiles16x16.png"
}, {
	name: "city",
	type: "image",
	src: "data/tilesets/city.png"
}, {
    name: "prehistory_1",
    type: "tmx",
    src: "data/maps/prehistory_1.tmx"
}, {
    name: "prehistory_2",
    type: "tmx",
    src: "data/maps/prehistory_2.tmx"
},{
    name: "prehistory_3",
    type: "tmx",
    src: "data/maps/prehistory_3.tmx"
}, {
    name: "middle_age_1",
    type: "tmx",
    src: "data/maps/middle_age_1.tmx"
},{
    name: "middle_age_1_1",
    type: "tmx",
    src: "data/maps/middle_age_1_1.tmx"
}, {
    name: "middle_age_2",
    type: "tmx",
    src: "data/maps/middle_age_2.tmx"
}, {
    name: "middle_age_3",
    type: "tmx",
    src: "data/maps/middle_age_3.tmx"
}, {
    name: "city_shop_1",
    type: "tmx",
    src: "data/maps/city_shop_1.tmx"
}, {
	name: "modern_1",
	type: "tmx",
	src: "data/maps/modern_1.tmx"
}, 
{
	name: "modern_1_1",
	type: "tmx",
	src: "data/maps/modern_1_1.tmx"
},{
	name: "end",
	type: "tmx",
	src: "data/maps/end.tmx"
},
{
	name: "secret_end",
	type: "tmx",
	src: "data/maps/secret_end.tmx"
}, {
    name: "charactertest",
    type: "image",
    src: "data/sprites/character.png"
}, {
    name: "caveman",
    type: "image",
    src: "data/sprites/caveman.png"
}, {
    name: "peasant",
    type: "image",
    src: "data/sprites/peasant.png"
}, {
	name: "modernman",
	type: "image",
	src: "data/sprites/modernman.png"
}, {
    name: "32x32_font",
    type: "image",
    src: "fonts/32x32_font.png"
},
 { /* Ennemy */
    name: "chicken",
    type: "image",
    src: "data/sprites/chicken.png"
},{ /* Ennemy */
    name: "dog",
    type: "image",
    src: "data/sprites/dog.png"
},{ 
    name: "RedDino",
    type: "image",
    src: "data/sprites/redDino.png"
}, 
{ 
    name: "BlueDino",
    type: "image",
    src: "data/sprites/blueDino.png"
},
{ 
    name: "Ptero",
    type: "image",
    src: "data/sprites/ptero.png"
},
{ 
    name: "Chopper",
    type: "image",
    src: "data/sprites/chopper.png"
},{
	/*BEGIN OFF ITEMS */
    name: "Pokeball",
    type: "image",
    src: "data/sprites/pokeball.png"
}];
 
var jsApp = {
    /* ---
 
     Initialize the jsApp
 
     --- */
    onload: function() {
 
        // init the video
        if (!me.video.init('game', 640, 480, false, 1.0)) {
            alert("Sorry but your browser does not support html 5 canvas.");
            return;
        }
		me.audio.enable();
        me.audio.init("mp3,ogg");
        me.loader.onload = this.loaded.bind(this);
        me.loader.preload(g_resources);
        me.state.change(me.state.LOADING);
		
		//Logs Init
		updateSave('Logs', 'Init Game <br />');

    },
    loaded: function() {
		//me.state.set(me.state.MENU, new TitleScreen());
        me.state.set(me.state.PLAY, new PlayScreen());
        me.entityPool.add("mainPlayer", Player);
		me.entityPool.add("RedDino", RedDinoEntity);
		me.entityPool.add("BlueDino", BlueDinoEntity);
		me.entityPool.add("Ptero", PteroEntity);
		me.entityPool.add("Chicken", ChickenEntity);
		me.entityPool.add("Dog", DogEntity);
		me.entityPool.add("Chopper", ChopperEntity);
		me.entityPool.add("InvisibleEntity", InvisibleEntity);
		me.entityPool.add("End", EndEntity);
		me.entityPool.add("Pokeball", ItemPokeball);
		me.entityPool.add("Warper", Warper);
		me.entityPool.add("DoDialog", DoDialog);
		me.input.bindKey(me.input.KEY.LEFT, "left");
		me.input.bindKey(me.input.KEY.RIGHT, "right");
		me.input.bindKey(me.input.KEY.ENTER, "use");
		me.input.triggerKeyEvent(me.input.KEY.ENTER, false);
		me.input.bindKey(me.input.KEY.UP, "jump", true);
        me.state.change(me.state.PLAY);
    },
	

};

var PlayScreen = me.ScreenObject.extend({
	init: function() {
		this.font = null;
	},
    onResetEvent: function() {
		game.ctx=document.getElementById('game').firstElementChild.getContext('2d');
		
        this.font = new me.BitmapFont("32x32_font", 32);
        this.font.set("left");
        me.levelDirector.loadLevel(getSave('MapArea')); //Le niveau est celui de la sauvegarde
       

		 // add a default HUD to the game mngr
        me.game.addHUD(0, 5, 640, 60);
 
        // add a new HUD item
        me.game.HUD.addItem("score", new ScoreObject(190, 5));
		me.game.HUD.setItemValue("score", getSave('Score')); // Loading saved score
		
		// add a new HUD item
        me.game.HUD.addItem("dieTxt", new ScoreObject(540, 5));
		me.game.HUD.setItemValue("dieTxt", 'DEATHS:'); 
		
		me.game.HUD.addItem("Death", new ScoreObject(620, 5));
		me.game.HUD.setItemValue("Death", getSave('Death')); // Loading saved nbr_morts
				
		 
        me.game.sort();
 
    },
    drawEpoq: function() {
		this.sfont.draw(game.ctx, game.curEpoq.name,10,10);
	
		
    },
	
	onUpdateFrameEvent:function(){
	
		this.drawEpoq();
	},
 
    /* ---
 
    action to perform when game is finished (state change)
 
    --- */
    onDestroyEvent: function() {
        // remove the HUD
        me.game.disableHUD();
    }
 
});

jsApp.onload();
