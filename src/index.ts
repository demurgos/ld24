import "./styles.scss";
import "./melonjs-patch";
import { me } from "melonjs";
import "melonjs/plugins/debug/debugPanel";
import { Game } from "./game/game";
import { Achievement, ALL_ACHIEVEMENTS } from "./game/achievements";

me.device.onReady(() => {
  const game = Game.fromSave();
  for (const a of ALL_ACHIEVEMENTS) {
    if (game.achievements.unlocked.has(a)) {
      unlockAchievement(a, false);
    }
  }
  game.onAchievement(a => unlockAchievement(a, true));
  game.onDialog(addDialog);
  game.onClearData(() => {
    const nodeList = document.querySelectorAll("#achievements li");
    for (let i = 0; i < nodeList.length; i++) {
      const li = nodeList[i];
      if (!(li instanceof HTMLLIElement)) {
        throw new Error("UnexpectedLi");
      }
      li.classList.remove("done");
    }
  });
  game.start();

  updateAudioControls();

  document.getElementById("mute")!.addEventListener("click", () => {
    game.mute();
    updateAudioControls();
  });

  document.getElementById("unmute")!.addEventListener("click", () => {
    game.unmute();
    updateAudioControls();
  });

  document.getElementById("restart")!.addEventListener("click", () => {
    game.restart(false);
  });

  document.getElementById("restart-clear")!.addEventListener("click", () => {
    game.restart(true);
  });

  function updateAudioControls() {
    if (game.isMuted()) {
      document.getElementById("mute")!.style.display = "none";
      document.getElementById("unmute")!.style.display = "inline-block";
    } else {
      document.getElementById("mute")!.style.display = "inline-block";
      document.getElementById("unmute")!.style.display = "none";
    }
  }
});

function unlockAchievement(achievement: Achievement, animate: boolean) {
  const nodeList = document.querySelectorAll("#achievements li");
  for (let i = 0; i < nodeList.length; i++) {
    const li = nodeList[i];
    if (!(li instanceof HTMLLIElement)) {
      throw new Error("UnexpectedLi");
    }
    if (li.dataset["achievement"] === achievement.key) {
      li.classList.add("done");
    }
  }
}

function addDialog(text: string) {
  const tNode = document.createTextNode(text);
  const liNode = document.createElement("li");
  liNode.appendChild(tNode);
  document.getElementById("logs")!.prepend(liNode);
}
