import { me } from "melonjs";

Object.defineProperty(me.Body.prototype, "respondToCollision", {writable: true});
Object.defineProperty(me.Entity.prototype, "update", {writable: true});
Object.defineProperty(me.Entity.prototype, "onCollision", {writable: true});
Object.defineProperty(me.Stage.prototype, "onResetEvent", {writable: true});
