import { Stats } from "./game";

const DINO_KILLS_FOR_METEOR: number = 30;
const CHICKEN_KILLS_FOR_H5N1: number = 10;
const DOG_KILLS_FOR_DOG: number = 20;
const KILLS_FOR_KILLER: number = 100;
const DEATHS_FOR_NOOB: number = 20;

export class Achievement {
  readonly key: string;
  readonly check: (stas: Readonly<Stats>) => boolean;
  constructor(key: string, check: (stas: Readonly<Stats>) => boolean) {
    this.key = key;
    this.check = check;
  }
}

const METEOR = new Achievement("meteor", s => s.dinoKills >= DINO_KILLS_FOR_METEOR);
const FIRE = new Achievement("fire", s => s.hasFire);
const WHEAT = new Achievement("wheat", s => s.hasWheat);
const H5N1 = new Achievement("h5n1", s => s.chickenKills >= CHICKEN_KILLS_FOR_H5N1);
const DOG = new Achievement("dog", s => s.dogKills >= DOG_KILLS_FOR_DOG);
const EXCALIBUR = new Achievement("excalibur", s => s.hasSword);
const BEER = new Achievement("beer", s => s.hasBeer);
const KILLER = new Achievement("killer", s => s.kills >= KILLS_FOR_KILLER);
const NOOB = new Achievement("noob", s => s.deaths >= DEATHS_FOR_NOOB);
const POKEBALL = new Achievement("pokeball", s => (s.hasPokeball1 || s.hasPokeball2 || s.hasPokeball3));

export const ALL_ACHIEVEMENTS: readonly Achievement[] = [
  METEOR,
  FIRE,
  WHEAT,
  H5N1,
  DOG,
  EXCALIBUR,
  BEER,
  KILLER,
  NOOB,
  POKEBALL,
];

export class Achievements {
  unlocked: Set<Achievement>;

  listeners: ((achievement: Achievement) => unknown)[];

  private constructor(unlocked: Set<Achievement>) {
    this.unlocked = unlocked;
    this.listeners = [];
  }

  public static empty(): Achievements {
    return new Achievements(new Set());
  }

  public static fromStats(stats: Readonly<Stats>): Achievements {
    const unlocked: Set<Achievement> = new Set();
    for (const achievement of ALL_ACHIEVEMENTS) {
      if (achievement.check(stats)) {
        unlocked.add(achievement);
      }
    }
    return new Achievements(unlocked);
  }

  public update(stats: Readonly<Stats>): void {
    for (const achievement of ALL_ACHIEVEMENTS) {
      if (this.unlocked.has(achievement)) {
        continue;
      }
      if (achievement.check(stats)) {
        this.handleUnlock(achievement);
      }
    }
  }

  public onAchievement(cb: (achievement: Achievement) => unknown) {
    this.listeners.push(cb);
  }

  private handleUnlock(achievement: Achievement) {
    this.unlocked.add(achievement);
    for(const cb of this.listeners) {
      cb(achievement);
    }
  }
}
