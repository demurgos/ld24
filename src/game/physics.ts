import { me } from "melonjs";
import { Player } from "./entities/player";

/**
 * From a collision between a player and ennemy, decide if the player wins.
 */
export function isPlayerWin(response: me.collision.ResponseObject): boolean {
  if (response.a instanceof Player) {
    return response.a.oldVelY > 1 || response.overlapV.y > 0;
  } else if (response.b instanceof Player) {
    return response.b.oldVelY > 1 || response.overlapV.y < 0;
  } else {
    throw new Error("NoPlayerInTheCollision");
  }
}

/**
 * Handle platforms being solid from one side, and other walls being solid.
 */
export function onWorldShapeCollision(
  ownBody: SolidBody,
  other: me.Entity,
  response: me.collision.ResponseObject
): boolean {
  if (other.height >= 1) {
    // Solid world shape
    return true;
  }
  // Platform
  if (ownBody.vel.y < 0) {
    return false;
  }
  // The check for the velocity is there to detect if we are likely to come from the top of the platform.
  if (response.overlapV.y > 0 && response.overlapV.y <= 2 * ownBody.vel.y) {
    response.overlapV.x = 0;
    return true;
  } else {
    return false;
  }
}

export class SolidBody extends me.Body {
  isOnGround: boolean;

  constructor(ancestor: me.Renderable) {
    super(ancestor);
    this.isOnGround = false;
  }

  respondToCollision(response: me.collision.ResponseObject): void {
    // Slop angle:
    // 0 means flat,
    // positive means going right-down
    // negative means going right-up
    // (The mismatched arguments are not an error but an application of the rotation)
    const slopeAngle = Math.atan2(response.overlapN.x, response.overlapN.y);
    // console.log(slopeAngle);

    // Math.PI / 4 would be enough, but we take slightly larger to avoid errors
    const GROUND_LIMIT = Math.PI / 3;
    const onGround = Math.abs(slopeAngle) < GROUND_LIMIT;

    const overlap: me.Vector2d = response.overlapV;

    if (onGround) {
      // On ground collision, only move the player up
      this.isOnGround = true;

      // Move out of the other object shape
      this.ancestor.pos.sub(new me.Vector2d(0, overlap.y));

      if (this.vel.y > 0) {
        this.vel.y = 0;
      }
    } else {
      // Move out of the other object shape
      this.ancestor.pos.sub(overlap);

      // adjust velocity
      if (overlap.x !== 0) {
        this.vel.x = ~~(0.5 + this.vel.x - overlap.x) || 0;
        if (this.bounce > 0) {
          this.vel.x *= -this.bounce;
        }
      }
      if (overlap.y !== 0) {
        this.vel.y = ~~(0.5 + this.vel.y - overlap.y) || 0;
        if (this.bounce > 0) {
          this.vel.y *= -this.bounce;
        }
      }
    }
  }
}
