import { me } from "melonjs";
import { Game } from "../game";

export class PlayScreen extends me.Stage {
  game: Game;

  constructor(game: Game) {
    super();
    this.game = game;
  }

  onResetEvent() {
    me.levelDirector.loadLevel(this.game.currentLevel);
  }
}
