import { me } from "melonjs";
import { Game } from "../game";
import { FAKE_FLOOR } from "../resources";
import { Player } from "./player";

export class FakeFloor extends me.CollectableEntity {
  static TYPE: string = "FakeFloor";

  static bind(game: Game): (x: number, y: number, settings: any) => FakeFloor {
    return (x: number, y: number, settings: any): FakeFloor => new FakeFloor(game, x, y, settings);
  }

  game: Game;

  constructor(game: Game, x: number, y: number, settings: any) {
    settings.image = FAKE_FLOOR.name;
    settings.framewidth = settings.width = 16;
    settings.frameheight = settings.height = 16;
    super(x, y, settings);
    this.game = game;
  }

  update(dt: number): boolean {
    return true;
  }

  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    if (other instanceof Player) {
      me.game.world.removeChild(this);
      this.body.setCollisionMask(me.collision.types.NO_OBJECT);
    }
    return false
  }
}
