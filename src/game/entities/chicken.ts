import { Game } from "../game";
import { CHICKEN } from "../resources";
import { Walker } from "./walker";

export class Chicken extends Walker {
  static TYPE: string = "Chicken";

  static bind(game: Game): (x: number, y: number, settings: any) => Chicken {
    return (x: number, y: number, settings: any): Chicken => new Chicken(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      {
        frameWidth: 64,
        frameHeight: 64,
        bodyX: 19,
        bodyY: 31,
        bodyWidth: 29,
        bodyHeight: 33,
        image: CHICKEN.name,
        velX: 3,
      },
    );
  }
}
