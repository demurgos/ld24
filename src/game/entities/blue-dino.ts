import { Game } from "../game";
import { BLUE_DINO } from "../resources";
import { Walker } from "./walker";

export class BlueDino extends Walker {
  static TYPE: string = "BlueDino";

  static bind(game: Game): (x: number, y: number, settings: any) => BlueDino {
    return (x: number, y: number, settings: any): BlueDino => new BlueDino(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      {
        frameWidth: 64,
        frameHeight: 64,
        bodyX: 19,
        bodyY: 31,
        bodyWidth: 25,
        bodyHeight: 33,
        image: BLUE_DINO.name,
        velX: 4,
      },
    );
  }
}
