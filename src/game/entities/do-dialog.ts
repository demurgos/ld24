import { me } from "melonjs";
import { Game } from "../game";

export class DoDialog extends me.CollectableEntity {
  static TYPE: string = "DoDialog";
  private game: Game;
  private dialogId: number;
  private dialogText: string;

  static bind(game: Game): (x: number, y: number, settings: any) => DoDialog {
    return (x: number, y: number, settings: any): DoDialog => new DoDialog(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(x, y, settings);
    this.game = game;
    this.dialogId = settings.id;
    this.dialogText = settings.text;
  }

  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    this.game.handleDialog(this.dialogId);
    this.body.setCollisionMask(me.collision.types.NO_OBJECT);
    me.game.world.removeChild(this);
    return false
  }
}
