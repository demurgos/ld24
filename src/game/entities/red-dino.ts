import { Game } from "../game";
import { RED_DINO } from "../resources";
import { Walker } from "./walker";

export class RedDino extends Walker {
  static TYPE: string = "RedDino";

  static bind(game: Game): (x: number, y: number, settings: any) => RedDino {
    return (x: number, y: number, settings: any): RedDino => new RedDino(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      {
        frameWidth: 64,
        frameHeight: 64,
        bodyX: 19,
        bodyY: 31,
        bodyWidth: 25,
        bodyHeight: 33,
        image: RED_DINO.name,
        velX: 3,
      },
    );
  }
}
