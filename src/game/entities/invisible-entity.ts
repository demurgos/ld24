import { me } from "melonjs";
import { Game } from "../game";
import { Player } from "./player";

export class InvisibleEntity extends me.Entity {
  static TYPE: string = "InvisibleEntity";

  static bind(game: Game): (x: number, y: number, settings: any) => InvisibleEntity {
    return (x: number, y: number, settings: any): InvisibleEntity => new InvisibleEntity(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(x, y, settings);
  }
  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    if (other instanceof Player) {
      other.die();
    }
    return false;
  }
}
