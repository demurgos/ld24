import { Game } from "../game";
import { DOG } from "../resources";
import { Walker } from "./walker";

export class Dog extends Walker {
  static TYPE: string = "Dog";

  static bind(game: Game): (x: number, y: number, settings: any) => Dog {
    return (x: number, y: number, settings: any): Dog => new Dog(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      {
        frameWidth: 64,
        frameHeight: 64,
        bodyX: 12,
        bodyY: 31,
        bodyWidth: 46,
        bodyHeight: 33,
        image: DOG.name,
        velX: 3,
      },
    );
  }
}
