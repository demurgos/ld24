import { me } from "melonjs";
import { Game } from "../game";
import { Player } from "./player";
import { PTERO, RED_DINO } from "../resources";
import { isPlayerWin, onWorldShapeCollision } from "../physics";

export interface WalkerOptions {
  frameWidth: number;
  frameHeight: number;
  bodyX: number;
  bodyY: number;
  bodyWidth: number;
  bodyHeight: number;
  image: string;
  velX: number;
}

export class Flyer extends me.Entity {
  xMin: number;
  xMax: number;
  dir: -1 | 1;
  game: Game;

  constructor(game: Game, x: number, y: number, settings: any, options: Readonly<WalkerOptions>) {
    const patrolLeft = x;
    const patrolRight = x + settings.width;
    const patrolBottom: number = y + settings.height;

    settings.image = options.image;
    settings.framewidth = settings.width = options.frameWidth;
    settings.frameheight = settings.height = options.frameHeight;

    super(x, y, settings);

    this.game = game;

    this.body = new me.Body(this);
    this.body.addShape(new me.Rect(0, 0, options.bodyWidth, options.bodyHeight));
    this.body.pos.set(-options.bodyX, -options.bodyY);

    this.body.maxVel.set(options.velX, 0);

    this.xMin = patrolLeft;
    this.xMax = patrolRight - options.bodyWidth;

    this.pos.x = this.xMax;
    this.pos.y = patrolBottom - options.bodyHeight;
    this.dir = -1;

    this.alive = true;
  }

  update(dt: number): boolean {
    if (this.alive) {
      if (this.dir < 0 && this.pos.x <= this.xMin) {
        this.dir = 1;
      } else if (this.dir > 0 && this.pos.x >= this.xMax) {
        this.dir = -1;
      }
      this.renderable.flipX(this.dir < 0);
      this.body.vel.x = this.dir * this.body.maxVel.x;
    } else {
      this.body.vel.x = 0;
    }
    this.body.update();
    me.collision.check(this);
    super.update(dt);
    return true;
  }

  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    if (other instanceof Player) {
      if (isPlayerWin(response)) {
        this.game.handleKill(this);
        me.game.world.removeChild(this);
      }
    }
    return false;
  }
}
