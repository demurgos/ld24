import { me } from "melonjs";
import { Game } from "../game";

export class Warper extends me.Entity {
  static TYPE: string = "Warper";

  static bind(game: Game): (x: number, y: number, settings: any) => Warper {
    return (x: number, y: number, settings: any): Warper => new Warper(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(x, y, settings);
  }
}
