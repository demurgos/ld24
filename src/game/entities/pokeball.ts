import { me } from "melonjs";
import { Game } from "../game";
import { EMPTY, POKEBALL } from "../resources";

export class Pokeball extends me.CollectableEntity {
  static TYPE: string = "Pokeball";

  static bind(game: Game): (x: number, y: number, settings: any) => Pokeball {
    return (x: number, y: number, settings: any): Pokeball => new Pokeball(game, x, y, settings);
  }

  game: Game;

  constructor(game: Game, x: number, y: number, settings: any) {
    if (game.hasCurrentPokeball()) {
      settings.image = EMPTY.name;
    } else {
      settings.image = POKEBALL.name;
    }
    settings.framewidth = settings.width = 16;
    settings.frameheight = settings.height = 16;
    super(x, y, settings);
    if (game.hasCurrentPokeball()) {
      this.body.setCollisionMask(me.collision.types.NO_OBJECT);
    }
    this.alwaysUpdate = true;
    this.game = game;
  }

  update(dt: number): boolean {
    if (this.game.hasCurrentPokeball()) {
      me.game.world.removeChild(this);
    }
    return true;
  }

  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    this.game.handlePokeball();
    this.body.setCollisionMask(me.collision.types.NO_OBJECT);
    return false
  }
}
