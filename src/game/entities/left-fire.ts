import { Game } from "../game";
import { EndFire } from "./end-fire";
import { me } from "melonjs";

export class LeftFire extends EndFire {
  static TYPE: string = "fwall";

  static bind(game: Game): (x: number, y: number, settings: any) => LeftFire {
    return (x: number, y: number, settings: any): LeftFire => new LeftFire(game, x, y, settings);
  }

  camVec: me.Vector2d;

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      true
    );
    this.camVec = new me.Vector2d();
    me.game.viewport.follow(this.camVec, me.game.viewport.AXIS.BOTH);
  }

  override update(dt: number): boolean {
    const res = super.update(dt);
    this.camVec.set(this.pos.x + 400, this.pos.y);
    return res;
  }
}
