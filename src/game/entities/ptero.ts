import { Game } from "../game";
import { PTERO } from "../resources";
import { Flyer } from "./flyer";

export class Ptero extends Flyer {
  static TYPE: string = "Ptero";

  static bind(game: Game): (x: number, y: number, settings: any) => Ptero {
    return (x: number, y: number, settings: any): Ptero => new Ptero(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      {
        frameWidth: 64,
        frameHeight: 64,
        bodyX: 11,
        bodyY: 31,
        bodyWidth: 37,
        bodyHeight: 20,
        image: PTERO.name,
        velX: 1.5,
      },
    );
  }
}
