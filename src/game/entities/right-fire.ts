import { Game } from "../game";
import { PTERO } from "../resources";
import { Flyer } from "./flyer";
import { EndFire } from "./end-fire";

export class RightFire extends EndFire {
  static TYPE: string = "rwall";

  static bind(game: Game): (x: number, y: number, settings: any) => RightFire {
    return (x: number, y: number, settings: any): RightFire => new RightFire(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      false,
    );
  }
}
