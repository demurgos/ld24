import { me } from "melonjs";
import { Action, Game, Stats } from "../game";
import { isPlayerWin, onWorldShapeCollision, SolidBody } from "../physics";
import { DEATH, END } from "../resources";
import { End } from "./end";

export enum Animation {
  Stand = "stand",
  Jump = "jump",
  Breath = "breath",
  Walk = "walk",
}

const FRAME_WIDTH: number = 64;
const FRAME_HEIGHT: number = 64;
const BODY_X: number = 25;
const BODY_Y: number = 33;
const BODY_WIDTH: number = 15;
const BODY_HEIGHT: number = 31;

function statsToMaxVel(stats: Readonly<Stats>): [number, number] {
  const vel: [number, number] = [4, 13];
  if (stats.hasFire) {
    vel[0] += 1;
  }
  if (stats.hasWheat) {
    vel[1] += 3;
  }
  if (stats.hasSword) {
    vel[0] += 1;
  }
  if (stats.hasBeer) {
    vel[1] += 2;
  }
  return vel;
}

export class Player extends me.Entity {
  static TYPE: string = "mainPlayer";

  static bind(game: Game): (x: number, y: number, settings: any) => Player {
    return (x: number, y: number, settings: any): Player => new Player(game, x, y, settings);
  }

  canBreakTile: boolean;
  renderable!: me.Sprite;
  body: SolidBody;
  oldCanJump: {can: boolean, dt: number}[];
  oldVelY: number;
  ladder: me.Renderable | null;
  game: Game;

  constructor(game: Game, x: number, y: number, settings: any) {
    const spawnLeft = x;
    const spawnRight = x + settings.width;
    const spawnBottom = y + settings.height;

    settings.framewidth = settings.width = FRAME_WIDTH;
    settings.frameheight = settings.height = FRAME_HEIGHT;
    super(x, y, settings);

    this.body = new SolidBody(this);
    this.body.addShape(new me.Rect(0, 0, BODY_WIDTH, BODY_HEIGHT));
    this.body.pos.set(-BODY_X, -BODY_Y);
    this.pos.set((spawnLeft + spawnRight - BODY_WIDTH) / 2, spawnBottom - BODY_HEIGHT - 1);
    this.body.maxVel.set(...statsToMaxVel(game.stats));
    this.body.friction.set(1, 0);

    if (game.currentLevel !== END.name) {
      me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
    }
    if (game.currentLevel === DEATH.name) {
      this.setOpacity(0);
      this.renderable.setOpacity(0);
    }

    this.alwaysUpdate = true;

    this.game = game;

    this.renderable.addAnimation(Animation.Stand, [0]);
    this.renderable.addAnimation(Animation.Jump, [3]);
    this.renderable.addAnimation(Animation.Breath, [0, 1, 2, 3]);
    this.renderable.addAnimation(Animation.Walk, [4, 5, 6, 7, 8]);
    this.renderable.setCurrentAnimation(Animation.Stand);

    this.ladder = null;

    this.oldVelY = 0;
    this.alive = true;
    this.canBreakTile = true;
    this.oldCanJump = [];
  }

  update(dt: number): boolean {
    if (this.game.currentLevel !== END.name) {
      me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
    }
    this.body.maxVel.set(...statsToMaxVel(this.game.stats));
    this.oldVelY = this.body.vel.y;
    this.oldCanJump.unshift({can: this.body.isOnGround, dt});
    this.body.isOnGround = false;
    let canJumpNow: boolean = false;
    let keepJumps: number = 0;
    let curDt: number = 0;
    const MAX_DT: number = 60;
    for (const {can, dt} of this.oldCanJump) {
      canJumpNow = canJumpNow || can;
      curDt += dt;
      if (curDt > MAX_DT) {
        break;
      } else {
        keepJumps += 1;
      }
    }
    this.oldCanJump.length = keepJumps;

    if (me.input.isKeyPressed(Action.Left)) {
      this.renderable.flipX(false);
      this.body.force.x = -this.body.maxVel.x;
      if (!this.renderable.isCurrentAnimation(Animation.Walk)) {
        this.renderable.setCurrentAnimation(Animation.Walk);
      }
      // this.body.vel.x = -5;
    } else if (me.input.isKeyPressed(Action.Right)) {
      // this.body.vel.x = 5;
      this.renderable.flipX(true);
      this.body.force.x = this.body.maxVel.x;
      if (!this.renderable.isCurrentAnimation(Animation.Walk)) {
        this.renderable.setCurrentAnimation(Animation.Walk);
      }
    } else {
      this.body.force.x = 0;
      this.renderable.setCurrentAnimation(Animation.Stand);
    }
    if (me.input.isKeyPressed(Action.Jump)) {
      if ((this.ladder !== null) && (this.pos.y > this.ladder.pos.y)) {
        this.body.vel.y = -4;
        this.body.force.y = 0;
      } else if ((canJumpNow || (this.ladder !== null)) && this.body.vel.y >= 0) {
        this.body.vel.y = -this.body.maxVel.y;
        this.body.force.y = -this.body.maxVel.y;
        this.oldCanJump.length = 0;
      } else {
        this.body.force.y = 0;
      }
    } else {
      if (this.ladder === null) {
        this.body.force.y = 0;
      } else {
        this.body.vel.y = 0.2;
        this.body.force.y = 0;
      }
    }
    this.body.update();
    this.ladder = null;
    me.collision.check(this);
    super.update(dt);
    return true;
  }

  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    if (other.body.collisionType === me.collision.types.WORLD_SHAPE) {
      return onWorldShapeCollision(this.body, other, response);
    }
    if (other.type === "ladder") {
      this.ladder = other;
      return false;
    }
    if (other instanceof End) {
      this.game.handleEnd();
      return false;
    }
    if (other.body.collisionType === me.collision.types.ENEMY_OBJECT && other.alive) {
      if (isPlayerWin(response)) {
        this.body.vel.y = -this.body.maxVel.y;
        this.body.force.y = -this.body.maxVel.y;
        this.oldCanJump.length = 0;
      } else {
        this.die();
      }
    }
    return false;
  }

  die() {
    this.alive = false;

    me.utils.function.defer(() => {
      me.levelDirector.reloadLevel()
    });
  }
}
