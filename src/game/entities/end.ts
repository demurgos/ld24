import { me } from "melonjs";
import { Game } from "../game";
import { Player } from "./player";

export class End extends me.CollectableEntity {
  static TYPE: string = "End";

  static bind(game: Game): (x: number, y: number, settings: any) => End {
    return (x: number, y: number, settings: any): End => new End(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(x, y, settings);
  }

  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    if (other instanceof Player) {
      this.body.setCollisionMask(me.collision.types.NO_OBJECT);
      me.game.world.removeChild(this);
    }
    return false
  }
}
