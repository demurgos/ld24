import { me } from "melonjs";
import { Game } from "../game";
import { Player } from "./player";
import { END_FIRE, PTERO, RED_DINO } from "../resources";
import { isPlayerWin, onWorldShapeCollision } from "../physics";

const WIDTH: number = 120;
const HEIGHT: number = 480;
const VEL = 1;

export interface EndFireOptions {
  velX: number;
}

export class EndFire extends me.Entity {
  // xMin: number;
  // xMax: number;
  // dir: -1 | 1;
  game: Game;

  constructor(game: Game, x: number, y: number, settings: any, isLeft: boolean) {
    settings.image = END_FIRE.name;
    settings.framewidth = settings.width = WIDTH;
    settings.frameheight = settings.height = HEIGHT;

    super(x, y, settings);

    this.game = game;

    this.body = new me.Body(this);
    this.body.addShape(new me.Rect(0, 0, WIDTH, HEIGHT));

    if (!isLeft) {
      this.renderable.flipX(true);
    }

    this.body.maxVel.set(VEL, 0);

    this.alive = true;
  }

  update(dt: number): boolean {
    this.body.vel.x = VEL;
    this.body.vel.y = 0;
    this.body.update();
    super.update(dt);
    return true;
  }

  onCollision(response: me.collision.ResponseObject, other: me.Entity): boolean {
    return false;
  }
}
