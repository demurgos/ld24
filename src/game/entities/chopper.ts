import { Game } from "../game";
import { CHOPPER } from "../resources";
import { Flyer } from "./flyer";

export class Chopper extends Flyer {
  static TYPE: string = "Chopper";

  static bind(game: Game): (x: number, y: number, settings: any) => Chopper {
    return (x: number, y: number, settings: any): Chopper => new Chopper(game, x, y, settings);
  }

  constructor(game: Game, x: number, y: number, settings: any) {
    super(
      game,
      x,
      y,
      settings,
      {
        frameWidth: 64,
        frameHeight: 64,
        bodyX: 11,
        bodyY: 31,
        bodyWidth: 45,
        bodyHeight: 27,
        image: CHOPPER.name,
        velX: 1.5,
      },
    );
  }
}
