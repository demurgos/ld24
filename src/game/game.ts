import { me } from "melonjs";
import {
  AUDIO_MAIN,
  AUDIO_MIDDLE_AGE, AUDIO_MODERN,
  AUDIO_PREHISTORY, CITY_SHOP_1, DEATH, END,
  MIDDLE_AGE_1,
  MIDDLE_AGE_1_1, MIDDLE_AGE_2, MIDDLE_AGE_3, MODERN_1, MODERN_1_1,
  PREHISTORY_1,
  PREHISTORY_2,
  PREHISTORY_3,
  RESOURCES, SECRET_END
} from "./resources";
import { BlueDino } from "./entities/blue-dino";
import { Chicken } from "./entities/chicken";
import { Chopper } from "./entities/chopper";
import { DoDialog } from "./entities/do-dialog";
import { End } from "./entities/end";
import { InvisibleEntity } from "./entities/invisible-entity";
import { Pokeball } from "./entities/pokeball";
import { Ptero } from "./entities/ptero";
import { RedDino } from "./entities/red-dino";
import { Warper } from "./entities/warper";
import { Dog } from "./entities/dog";
import { Player } from "./entities/player";
import { Achievement, Achievements } from "./achievements";
import { PlayScreen } from "./stages/play-screen";
import { DIALOG_EN } from "./dialog";
import { FakeFloor } from "./entities/fake-floor";
import { LeftFire } from "./entities/left-fire";
import { RightFire } from "./entities/right-fire";

const LOCALSTORAGE_KEY: string = "ld24_save";

export const enum Action {
  Left = "left",
  Right = "right",
  Use = "use",
  Jump = "jump",
}

export interface Stats {
  score: number;
  deaths: number;
  kills: number;
  dinoKills: number;
  chickenKills: number;
  dogKills: number;
  hasFire: boolean;
  hasSword: boolean;
  hasBeer: boolean;
  hasWheat: boolean;
  hasPokeball1: boolean;
  hasPokeball2: boolean;
  hasPokeball3: boolean;
}

export interface Save {
  stats: Stats;
  currentLevel: string;
  muted: boolean;
}

function upsertSave(save: Save): void {
  window.localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify({
    stats: save.stats,
    currentLevel: save.currentLevel,
    muted: save.muted,
  }));
}

function getSave(): Save | null {
  const saveJson: string | null = window.localStorage.getItem(LOCALSTORAGE_KEY);
  if (saveJson === null) {
    return null;
  }
  let rawSave: unknown;
  try {
    rawSave = JSON.parse(saveJson);
  } catch {
    console.error("CorruptedSaveFile:", saveJson);
    return null;
  }
  return rawSave as Save;
}

function getNewSave(): Save {
  return {
    currentLevel: PREHISTORY_1.name,
    stats: {
      score: 0,
      deaths: 0,
      kills: 0,
      dinoKills: 0,
      chickenKills: 0,
      dogKills: 0,
      hasFire: false,
      hasSword: false,
      hasBeer: false,
      hasWheat: false,
      hasPokeball1: false,
      hasPokeball2: false,
      hasPokeball3: false,
    },
    muted: false,
  };
}

export class Game implements Save {
  public currentLevel: string;
  public stats: Stats;
  public muted: boolean;

  public achievements: Achievements;

  private killSet: WeakSet<me.Renderable>;
  private loaded: boolean;
  private dialogSet: Set<number>;
  private dialogListeners: ((text: string) => unknown)[]
  private clearDataListeners: (() => unknown)[]

  private constructor(save: Save) {
    this.currentLevel = save.currentLevel;
    this.stats = save.stats;
    this.muted = save.muted;
    this.achievements = Achievements.fromStats(this.stats);
    this.killSet = new WeakSet();
    this.loaded = false;
    this.dialogSet = new Set();
    this.dialogListeners = [];
    this.clearDataListeners = [];
    if (this.muted) {
      me.audio.muteAll();
    }
  }

  public static fromSave(): Game {
    let save: Save | null = getSave();
    if (save === null) {
      save = getNewSave();
    }
    return new Game(save);
  }

  mute(): void {
    this.muted = true;
    upsertSave(this);
    me.audio.muteAll();
  }

  unmute(): void {
    this.muted = false;
    upsertSave(this);
    me.audio.unmuteAll();
  }

  isMuted(): boolean {
    return this.muted;
  }

  restart(clearSave: boolean): void {
    me.utils.function.defer(() => {
      const fresh = getNewSave();
      this.currentLevel = fresh.currentLevel;
      this.dialogSet = new Set();
      if (clearSave) {
        this.stats = fresh.stats;
        const onAchievementListeners = this.achievements.listeners;
        this.achievements = Achievements.fromStats(this.stats);
        this.achievements.listeners = onAchievementListeners;
        for (const cb of this.clearDataListeners) {
          cb();
        }
      }
      upsertSave(this);
      if (this.loaded) {
        me.levelDirector.loadLevel(this.currentLevel);
      }
    })
  }

  start() {
    if (!me.video.init(640, 480, {
      consoleHeader: false,
      parent: "game",
      preferWebGL1: false,
      scale: "auto",
      scaleMethod: "flex-width"
    })) {
      alert("Failed to initialize video, probably due to browser compatibility.");
      return;
    }
    if (!me.audio.init("mp3,ogg")) {
      alert("Failed to initialize audio, probably due to browser compatibility.");
      return;
    }
    me.loader.preload(RESOURCES.getAll(), () => this.handleLoaded());
    me.event.subscribe(me.event.LEVEL_LOADED, (id: string) => this.handleLevelChange(id));
    me.state.change(me.state.LOADING);
  }

  handleLoaded() {
    this.loaded = true;
    me.state.set(me.state.PLAY, new PlayScreen(this));

    me.pool.register(BlueDino.TYPE, BlueDino.bind(this), false);
    me.pool.register(Chicken.TYPE, Chicken.bind(this), false);
    me.pool.register(Chopper.TYPE, Chopper.bind(this), false);
    me.pool.register(Chopper.TYPE, Chopper.bind(this), false);
    me.pool.register(DoDialog.TYPE, DoDialog.bind(this), false);
    me.pool.register(Dog.TYPE, Dog.bind(this), false);
    me.pool.register(End.TYPE, End.bind(this), false);
    me.pool.register(InvisibleEntity.TYPE, InvisibleEntity.bind(this), false);
    me.pool.register(Player.TYPE, Player.bind(this), false);
    me.pool.register(Pokeball.TYPE, Pokeball.bind(this), false);
    me.pool.register(Ptero.TYPE, Ptero.bind(this), false);
    me.pool.register(RedDino.TYPE, RedDino.bind(this), false);
    me.pool.register(Warper.TYPE, Warper.bind(this), false);
    me.pool.register(FakeFloor.TYPE, FakeFloor.bind(this), false);
    me.pool.register(LeftFire.TYPE, LeftFire.bind(this), false);
    me.pool.register(RightFire.TYPE, RightFire.bind(this), false);

    me.input.bindKey(me.input.KEY.LEFT, Action.Left);
    me.input.bindKey(me.input.KEY.RIGHT, Action.Right);
    me.input.bindKey(me.input.KEY.ENTER, Action.Use);
    me.input.bindKey(me.input.KEY.UP, Action.Jump);

    me.input.triggerKeyEvent(me.input.KEY.ENTER, false);

    me.state.change(me.state.PLAY);
  }

  handleKill(entity: me.Renderable): void {
    if (this.killSet.has(entity)) {
      return;
    }
    this.killSet.add(entity);
    if (entity instanceof RedDino) {
      this.stats.score += 100;
      this.stats.kills += 1;
      this.stats.dinoKills += 1;
    } else if (entity instanceof BlueDino) {
      this.stats.score += 200;
      this.stats.kills += 1;
      this.stats.dinoKills += 1;
    } else if (entity instanceof Ptero) {
      this.stats.score += 300;
      this.stats.kills += 1;
      this.stats.dinoKills += 1;
    } else if (entity instanceof Chicken) {
      this.stats.score += 200;
      this.stats.kills += 1;
      this.stats.chickenKills += 1;
    } else if (entity instanceof Dog) {
      this.stats.score += 300;
      this.stats.kills += 1;
      this.stats.dogKills += 1;
    } else if (entity instanceof Chopper) {
      this.stats.score += 300;
      this.stats.kills += 1;
    } else {
      console.warn("UnknownEntityKill");
    }
    this.updateAchievements();
    upsertSave(this);
  }

  handleLevelChange(levelId: string): void {
    this.currentLevel = levelId;
    const wantedTrack = levelToTrackName(this.currentLevel);
    if (me.audio.getCurrentTrack() !== wantedTrack) {
      me.audio.stopTrack();
      me.audio.playTrack(wantedTrack);
    }
    switch (levelId) {
      case PREHISTORY_2.name:
        if (!this.stats.hasFire) {
          this.stats.hasFire = true;
          this.stats.score += 1000;
        }
        break;
      case PREHISTORY_3.name:
        if (!this.stats.hasWheat) {
          this.stats.hasWheat = true;
          this.stats.score += 2000;
        }
        break;
      case MIDDLE_AGE_2.name:
        if (!this.stats.hasSword) {
          this.stats.hasSword = true;
          this.stats.score += 1000;
        }
        break;
      case MIDDLE_AGE_3.name:
        if (!this.stats.hasBeer) {
          this.stats.hasBeer = true;
          this.stats.score += 2000;
        }
        break;
      case END.name:
        // score += 2000
        break;
      case SECRET_END.name:
        // score += 10000
        break;
      default:
        break;
    }
    this.updateAchievements();
    upsertSave(this);
  }

  handleDialog(id: number): void {
    if (this.dialogSet.has(id)) {
      return;
    }
    this.dialogSet.add(id);
    const text = DIALOG_EN.get(id);
    if (text === undefined) {
      throw new Error(`DialogNotFound: ${id}`);
    }
    for (const listener of this.dialogListeners) {
      listener(text);
    }
  }

  handlePokeball(): void {
    switch (this.currentLevel) {
      case PREHISTORY_1.name:
        this.stats.hasPokeball1 = true;
        break;
      case MIDDLE_AGE_2.name:
        this.stats.hasPokeball2 = true;
        break;
      case CITY_SHOP_1.name:
        this.stats.hasPokeball3 = true;
        break;
      default:
        throw new Error("UnexpectedPokeballLevel");
    }
    this.updateAchievements();
    upsertSave(this);
  }

  handleEnd(): void {
    me.utils.function.defer(() => {
      if (this.stats.hasPokeball1 && this.stats.hasPokeball2 && this.stats.hasPokeball3) {
        me.levelDirector.loadLevel(SECRET_END.name);
      } else {
        me.levelDirector.loadLevel(DEATH.name);
      }
    });
  }

  hasCurrentPokeball(): boolean {
    switch (this.currentLevel) {
      case PREHISTORY_1.name:
        return this.stats.hasPokeball1;
      case MIDDLE_AGE_2.name:
        return this.stats.hasPokeball2;
      case CITY_SHOP_1.name:
        return this.stats.hasPokeball3;
      default:
        return false;
    }
  }

  updateAchievements(): void {
    this.achievements.update(this.stats);
  }

  onAchievement(cb: (achievement: Achievement) => unknown) {
    this.achievements.onAchievement(cb);
  }

  onDialog(cb: (text: string) => unknown) {
    this.dialogListeners.push(cb);
  }

  onClearData(cb: () => unknown) {
    this.clearDataListeners.push(cb);
  }
}

export function levelToTrackName(levelName: string): string {
  switch (levelName) {
    case PREHISTORY_1.name:
    case PREHISTORY_2.name:
    case PREHISTORY_3.name:
      return AUDIO_PREHISTORY.name;
    case MIDDLE_AGE_1.name:
    case MIDDLE_AGE_1_1.name:
    case MIDDLE_AGE_2.name:
    case MIDDLE_AGE_3.name:
      return AUDIO_MIDDLE_AGE.name;
    case MODERN_1.name:
    case MODERN_1_1.name:
    case END.name:
    case DEATH.name:
    case CITY_SHOP_1.name:
      return AUDIO_MODERN.name;
    case SECRET_END.name:
    default:
      return AUDIO_MAIN.name;
  }
}
