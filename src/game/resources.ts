export type ResourceType = "audio" | "binary" | "fontface" | "image" | "js" | "json" | "tmx" | "tsx";

export class ResourceRef<Name extends string = string> {
  readonly name: Name;
  readonly type: ResourceType;
  readonly src: string;

  private constructor(name: Name, type: ResourceType, src: string) {
    this.name = name;
    this.type = type;
    // TODO: Do not hard-code absolute URI
    this.src = `./data/${src}`;
  }

  static audio<N extends string>(name: N, src: string): ResourceRef<N> {
    return new ResourceRef(name, "audio", src);
  }

  static image<N extends string>(name: N, src: string): ResourceRef<N> {
    return new ResourceRef(name, "image", src);
  }

  static tmx<N extends string>(name: N, src: string): ResourceRef<N> {
    return new ResourceRef(name, "tmx", src);
  }
}

export class RessourceRegistry {
  private readonly ressources: ResourceRef[];

  public constructor() {
    this.ressources = [];
  }

  audio<N extends string>(name: N, src: string): ResourceRef<N> {
    const ressource = ResourceRef.audio(name, src);
    this.ressources.push(ressource);
    return ressource;
  }

  image<N extends string>(name: N, src: string): ResourceRef<N> {
    const ressource = ResourceRef.image(name, src);
    this.ressources.push(ressource);
    return ressource;
  }

  tmx<N extends string>(name: N, src: string): ResourceRef<N> {
    const ressource = ResourceRef.tmx(name, src);
    this.ressources.push(ressource);
    return ressource;
  }

  getAll(): ResourceRef[] {
    return [...this.ressources];
  }
}

export const RESOURCES: RessourceRegistry = new RessourceRegistry();

export const DIRT = RESOURCES.image("dirt", "tilesets/dirt.png");
export const ITEMS = RESOURCES.image("items", "tilesets/items.png");
export const DIVERSE = RESOURCES.image("diverse", "tilesets/diverse.png");
export const FIELD = RESOURCES.image("field", "tilesets/field.png");
export const FIRE = RESOURCES.image("fire", "tilesets/fire.png");
export const GOLD_BRICKS = RESOURCES.image("GoldBricks", "tilesets/GoldBricks.png");
export const MISC_SCENERY = RESOURCES.image("misc_scenery", "tilesets/misc_scenery.png");
export const PLATFORM = RESOURCES.image("platform", "tilesets/platform.png");
export const GRASS = RESOURCES.image("grass", "tilesets/grass.png");
export const GRASSY_GROUND = RESOURCES.image("grassy-ground", "tilesets/grassy-ground.png");
export const TILESET = RESOURCES.image("tileset", "tilesets/tileset.gif");
export const INDUSTRIAL = RESOURCES.image("industrial", "tilesets/industrial.png");
export const METAL = RESOURCES.image("metal", "tilesets/metal.png");
export const FURNITURE = RESOURCES.image("furniture", "tilesets/furniture.png");
export const METATILES = RESOURCES.image("metatiles16x16", "tilesets/metatiles16x16.png");
export const CITY = RESOURCES.image("city", "tilesets/city.png");
export const PREHISTORY_1 = RESOURCES.tmx("prehistory_1", "maps/prehistory_1.tmx");
export const PREHISTORY_2 = RESOURCES.tmx("prehistory_2", "maps/prehistory_2.tmx");
export const PREHISTORY_3 = RESOURCES.tmx("prehistory_3", "maps/prehistory_3.tmx");
export const MIDDLE_AGE_1 = RESOURCES.tmx("middle_age_1", "maps/middle_age_1.tmx");
export const MIDDLE_AGE_1_1 = RESOURCES.tmx("middle_age_1_1", "maps/middle_age_1_1.tmx");
export const MIDDLE_AGE_2 = RESOURCES.tmx("middle_age_2", "maps/middle_age_2.tmx");
export const MIDDLE_AGE_3 = RESOURCES.tmx("middle_age_3", "maps/middle_age_3.tmx");
export const CITY_SHOP_1 = RESOURCES.tmx("city_shop_1", "maps/city_shop_1.tmx");
export const MODERN_1 = RESOURCES.tmx("modern_1", "maps/modern_1.tmx");
export const MODERN_1_1 = RESOURCES.tmx("modern_1_1", "maps/modern_1_1.tmx");
export const END = RESOURCES.tmx("end", "maps/end.tmx");
export const SECRET_END = RESOURCES.tmx("secret_end", "maps/secret_end.tmx");
export const DEATH = RESOURCES.tmx("death", "maps/death.tmx");
export const CHARACTERTEST = RESOURCES.image("charactertest", "sprites/character.png");
export const CAVEMAN = RESOURCES.image("caveman", "sprites/caveman.png");
export const PEASANT = RESOURCES.image("peasant", "sprites/peasant.png");
export const MODERNMAN = RESOURCES.image("modernman", "sprites/modernman.png");
export const FONT32 = RESOURCES.image("32x32_font", "fonts/32x32_font.png");
export const CHICKEN = RESOURCES.image("chicken", "sprites/chicken.png");
export const END_FIRE = RESOURCES.image("end_fire", "sprites/fwall.png");
export const DOG = RESOURCES.image("dog", "sprites/dog.png");
export const RED_DINO = RESOURCES.image("redDino", "sprites/redDino.png");
export const BLUE_DINO = RESOURCES.image("blueDino", "sprites/blueDino.png");
export const PTERO = RESOURCES.image("ptero", "sprites/ptero.png");
export const CHOPPER = RESOURCES.image("chopper", "sprites/chopper.png");
export const POKEBALL = RESOURCES.image("Pokeball", "sprites/pokeball.png");
export const FAKE_FLOOR = RESOURCES.image("FakeFloor", "sprites/fake_floor.png");
export const EMPTY = RESOURCES.image("empty", "sprites/empty.png");
export const AUDIO_MAIN = RESOURCES.audio("main", "audio/");
export const AUDIO_MIDDLE_AGE = RESOURCES.audio("middle_age", "audio/");
export const AUDIO_MODERN = RESOURCES.audio("modern", "audio/");
export const AUDIO_PREHISTORY = RESOURCES.audio("prehistory", "audio/");
