declare module "melonjs" {
  export namespace me {
    export namespace audio {
      export function disable(): void;

      export function enabled(): void;

      export function fade(soundName: string, from: number, to: number, duration: number, id?: number): void;

      export function getCurrentTrack(): string;

      export function getVolume(): number;

      export function hasAudio(): void;

      export function init(audioFormat: string): boolean;

      export function muteAll(): void;

      export function muted(): boolean;

      export function pause(soundName: string, id?: number): void;

      export function pauseTrack(): void;

      export function play(soundName: string, loop?: boolean, onEnd?: any, volume?: number): number;

      export function playTrack(soundName: string, volume?: number): number;

      export function rate(soundName: string, rate?: number, id?: number): number;

      export function resume(soundName: string, id?: number): void;

      export function resumeTrack(): void;

      export function seek(soundName: string, seek?: number, id?: number): void;

      export function setVolume(volume: number): void;

      export function stop(soundName?: string, id?: number): void;

      export function stopTrack(): void;

      export function unload(soundName: string): boolean;

      export function unloadAll(): void;

      export function unmute(soundName: string, id?: number): void;

      export function unmuteAll(): void;
    }

    export namespace collision {
      export const maxChildren: number;
      export const maxDepth: number;
      export const response: ResponseObject;

      export class ResponseObject {
        a: Renderable;
        b: Renderable;
        overlap: number;
        overlapV: Vector2d;
        overlapN: Vector2d;
        aInB: boolean;
        bInA: boolean;
        indexShapeA: number;
        indexShapeB: number;
      }

      export namespace types {
        export const NO_OBJECT: number;
        export const PLAYER_OBJECT: number;
        export const NPC_OBJECT: number;
        export const ENEMY_OBJECT: number;
        export const COLLECTABLE_OBJECT: number;
        export const ACTION_OBJECT: number;
        export const PROJECTILE_OBJECT: number;
        export const WORLD_SHAPE: number;
        export const USER: number;
        export const ALL_OBJECT: number;
      }

      export function check(obj: Renderable, resp?: ResponseObject): boolean;

      export function rayCast(line: Line, resp?: ResponseObject): Renderable[];

      export function shouldCollide(a: Renderable, b: Renderable): boolean;
    }

    export namespace device {
      export function onReady(cb: () => void): void;
    }

    export namespace event {
      export type ChannelName = string;

      export const CANVAS_ONRESIZE: ChannelName;
      export const DRAGEND: ChannelName;
      export const DRAGSTART: ChannelName;
      export const GAME_INIT: ChannelName;
      export const GAME_RESET: ChannelName;
      export const GAME_UPDATE: ChannelName;
      export const GAMEPAD_CONNECTED: ChannelName;
      export const GAMEPAD_DISCONNECTED: ChannelName;
      export const GAMEPAD_UPDATE: ChannelName;
      export const KEYDOWN: ChannelName;
      export const KEYUP: ChannelName;
      export const LEVEL_LOADED: ChannelName;
      export const LOADER_COMPLETE: ChannelName;
      export const LOADER_PROGRESS: ChannelName;
      export const POINTER_MOVE: ChannelName;
      export const STATE_PAUSE: ChannelName;
      export const STATE_RESTART: ChannelName;
      export const STATE_RESUME: ChannelName;
      export const STATE_STOP: ChannelName;
      export const VIDEO_INIT: ChannelName;
      export const VIEWPORT_ONCHANGE: ChannelName;
      export const VIEWPORT_ONRESIZE: ChannelName;
      export const WEBGL_ONCONTEXT_LOST: ChannelName;
      export const WEBGL_ONCONTEXT_RESTORED: ChannelName;
      export const WEBGL_ONORIENTATION_CHANGE: ChannelName;
      export const WINDOW_ONRESIZE: ChannelName;
      export const WINDOW_ONSCROLL: ChannelName;

      export function publish(channel: ChannelName, ...args: readonly unknown[]): void;
      export function subscribe(channel: ChannelName, cb: any): any;
      export function unsubscribe(handle: any, cb?: any): void;
    }

    export namespace game {
      export const HASH: any;
      export const mergeGroup: boolean;
      export const sortOn: "x" | "y" | "z";
      export const viewport: Camera2d;
      export const world: World;

      export function getParentContainer(child: Renderable): Container;

      export function onLevelLoaded(): void;

      export function repaint(): void;

      export function reset(): void;

      export function updateFrameRate(): void;
    }

    export namespace input {
      export type KeyCode = number;

      export namespace KEY {
        export const A: KeyCode;
        export const B: KeyCode;
        export const C: KeyCode;
        export const D: KeyCode;
        export const E: KeyCode;
        export const F: KeyCode;
        export const G: KeyCode;
        export const H: KeyCode;
        export const I: KeyCode;
        export const J: KeyCode;
        export const K: KeyCode;
        export const L: KeyCode;
        export const M: KeyCode;
        export const N: KeyCode;
        export const O: KeyCode;
        export const P: KeyCode;
        export const Q: KeyCode;
        export const R: KeyCode;
        export const S: KeyCode;
        export const T: KeyCode;
        export const U: KeyCode;
        export const V: KeyCode;
        export const W: KeyCode;
        export const X: KeyCode;
        export const Y: KeyCode;
        export const Z: KeyCode;

        export const F1: KeyCode;
        export const F2: KeyCode;
        export const F3: KeyCode;
        export const F4: KeyCode;
        export const F5: KeyCode;
        export const F6: KeyCode;
        export const F7: KeyCode;
        export const F8: KeyCode;
        export const F9: KeyCode;
        export const F10: KeyCode;
        export const F11: KeyCode;
        export const F12: KeyCode;

        export const NUM0: KeyCode;
        export const NUM1: KeyCode;
        export const NUM2: KeyCode;
        export const NUM3: KeyCode;
        export const NUM4: KeyCode;
        export const NUM5: KeyCode;
        export const NUM6: KeyCode;
        export const NUM7: KeyCode;
        export const NUM8: KeyCode;
        export const NUM9: KeyCode;

        export const NUMPAD0: KeyCode;
        export const NUMPAD1: KeyCode;
        export const NUMPAD2: KeyCode;
        export const NUMPAD3: KeyCode;
        export const NUMPAD4: KeyCode;
        export const NUMPAD5: KeyCode;
        export const NUMPAD6: KeyCode;
        export const NUMPAD7: KeyCode;
        export const NUMPAD8: KeyCode;
        export const NUMPAD9: KeyCode;

        export const ADD: KeyCode;
        export const ALT: KeyCode;
        export const BACK_SLASH: KeyCode;
        export const BACKSPACE: KeyCode;
        export const CAPS_LOCK: KeyCode;
        export const CLOSE_BRACKET: KeyCode;
        export const COMMA: KeyCode;
        export const CTRL: KeyCode;
        export const DECIMAL: KeyCode;
        export const DELETE: KeyCode;
        export const DIVIDE: KeyCode;
        export const DOWN: KeyCode;
        export const END: KeyCode;
        export const ENTER: KeyCode;
        export const ESC: KeyCode;
        export const FORWARD_SLASH: KeyCode;
        export const GRAVE_ACCENT: KeyCode;
        export const HOME: KeyCode;
        export const INSERT: KeyCode;
        export const LEFT: KeyCode;
        export const MINUS: KeyCode;
        export const MULTIPLY: KeyCode;
        export const NUM_LOCK: KeyCode;
        export const OPEN_BRACKET: KeyCode;
        export const PAGE_DOWN: KeyCode;
        export const PAGE_UP: KeyCode;
        export const PAUSE: KeyCode;
        export const PERIOD: KeyCode;
        export const PLUS: KeyCode;
        export const PRINT_SCREEN: KeyCode;
        export const RIGHT: KeyCode;
        export const SCROLL_LOCK: KeyCode;
        export const SEMICOLON: KeyCode;
        export const SHIFT: KeyCode;
        export const SINGLE_QUOTE: KeyCode;
        export const SPACE: KeyCode;
        export const SUBSTRACT: KeyCode;
        export const TAB: KeyCode;
        export const TILDE: KeyCode;
        export const UP: KeyCode;
        export const WINDOW_KEY: KeyCode;
      }

      export function bindKey(keyCode: KeyCode, action: string, lock?: boolean, preventDefault?: boolean): void;

      export function isKeyPressed(action: string): boolean;

      export function triggerKeyEvent(keyCode: KeyCode, status: boolean): void;
    }

    export namespace levelDirector {
      export function getCurrentLevel(): TMXTileMap;
      export function getCurrentLevelId(): string;
      export function levelCount(): number;
      export function loadLevel(level: string, options?: object): void;
      export function nextLevel(options?: object): void;
      export function previousLevel(options?: object): void;
      export function reloadLevel(options?: object): void;
    }

    export namespace loader {
      export function preload(resources: readonly object[], cb: () => void): void;
    }

    export namespace pool {
      export type Factory = (x: number, y: number, settings: any) => Renderable;

      export function register(name: string, cls: Factory, objectPooling?: boolean): void;
    }

    export namespace state {
      export type StateId = number;

      export const CREDITS: StateId;
      export const GAME_END: StateId;
      export const GAMEOVER: StateId;
      export const LOADING: StateId;
      export const MENU: StateId;
      export const PLAY: StateId;
      export const READY: StateId;
      export const SCORE: StateId;
      export const SETTINGS: StateId;
      export const USER: StateId;

      export function change(state: StateId, ...args: readonly unknown[]): void;
      export function change(state: StateId, forceChange: boolean, ...args: readonly unknown[]): void;

      export function set(state: StateId, stage: Stage, options?: object): void;
    }

    export const utils: {
      "function": {
        defer: (cb: any) => void;
      }
    };

    export namespace video {
      export type RendererId = number;

      export const AUTO: RendererId;
      export const CANVAS: RendererId;
      export const WEBGL: RendererId;

      export function createCanvas(
        width: number,
        height: number,
        offScreen?: boolean
      ): HTMLCanvasElement /* | OffscreenCanvas */;

      export interface InitOptions {
        parent?: string | HTMLElement;
        renderer?: RendererId;
        doubleBuffering?: boolean;
        scale?: number | string;
        scaleMethod?: string;
        preferWebGL1?: boolean;
        powerPreference?: string;
        transparent?: boolean;
        antiAlias?: boolean;
        consoleHeader?: boolean;
      }

      export function init(width: number, height: number, options: Readonly<InitOptions>): boolean;
    }

    export class Body extends Rect {
      constructor(ancestor: Renderable, shapes?: any, onBodyUpdate?: any);

      accel: Vector2d;
      bounce: number;
      collisionType: number;
      readonly falling: boolean;
      force: Vector2d;
      friction: Vector2d;
      gravity: Vector2d;
      gravityScale: number;
      ignoreGravity: boolean;
      readonly jumping: boolean;
      mass: number;
      maxVel: Vector2d;
      vel: Vector2d;
      ancestor: Renderable;

      addShape(shape: Rect | Polygon | Line | Ellipse, batchInsert?: boolean): number;

      addShapeFromJson(shape: Rect | Polygon | Line | Ellipse | object, batchInsert: boolean): number;

      fromJson(json: object, id?: string): number;

      getShape(index?: number): Polygon | Line | Ellipse;

      removeShape(shape: Polygon | Line | Ellipse): number;

      removeShapeAt(index: number): number;

      respondToCollision(response: collision.ResponseObject): void;

      setCollisionMask(bitmask: number): void;

      setFriction(x: number, y: number): void;

      setMaxVelocity(x: number, y: number): void;

      setVelocity(x: number, y: number): void;

      setVertices(vertices: Vector2d[], index?: number): void;

      update(): boolean;
    }

    export class Camera2d {
      readonly AXIS: {
        readonly NONE: number;
        readonly HORIZONTAL: number;
        readonly VERTICAL: number;
        readonly BOTH: number;
      };

      constructor(minX: number, minY: number, maxX: number, maxY: number);

      follow(target: Renderable | Vector2d, axis?: number, damping?: number): void;
    }

    export class CanvasRenderer {
      constructor(options: CanvasRenderer.Options);
    }

    namespace CanvasRenderer {
      export interface Options extends Renderer.Options {
        // TODO
      }
    }

    export class CollectableEntity extends Entity {
      constructor(x: number, y: number, settings: any);
    }

    export class Color {
    }

    export class Container extends Renderable {
      removeChild(child: Renderable, keepalive?: boolean): void;

      removeChildNow(child: Renderable, keepalive?: boolean): void;
    }

    export class Ellipse {
    }

    export class Entity extends Renderable {
      alive: boolean;
      body: Body;
      id: number;
      renderable: Renderable;
      type: string;

      constructor(x: number, y: number, settings: any);

      onCollision(response: collision.ResponseObject, other: Entity): boolean;
    }

    export class Line {
    }

    export class Matrix2d {
    }

    export class ObservableVector2d {
    }

    export class ObservableVector3d extends Vector3d {
    }

    export class Polygon {
      points: Vector2d[];
      pos: Vector2d;

      clone(): this;

      containsPoint(x: number, y: number): boolean;

      containsPointV(point: Vector2d): boolean;

      getBounds(): Rect;

      getIndices(a: readonly Vector2d[]): this;

      recalc(): this;

      rotate(angle: number, v?: Vector2d | ObservableVector2d): this;

      scale(x: number, y?: number): this;

      scaleV(v: Vector2d): this;

      to2d(): this;

      toIso(): this;

      transform(matrix: Matrix2d): this;

      translate(x: number, y: number): this;

      translateV(v: Vector2d): this;

      updateBounds(): Rect;
    }

    export class Rect extends Polygon {
      constructor(x: number, y: number, w: number, h: number);

      bottom: number;
      center: Vector2d;
      centerX: number;
      centerY: number;
      height: number;
      left: number;
      right: number;
      top: number;
      width: number;

      contains(rect: Rect): boolean;

      copy(rect: Rect): Rect;

      equals(rect: Rect): boolean;

      isFinite(): boolean;

      overlaps(rect: Rect): boolean;

      resize(w: number, h: number): boolean;

      setPoints(points: Vector2d[]): Rect;

      toPolygon(): Polygon;

      union(rect: Rect): Rect;

      updateBounds(): Rect;
    }

    export class Renderable extends Rect {
      alwaysUpdate: boolean;
      autoTransform: boolean;
      floating: boolean;
      guid: string;
      inViewport: boolean;
      isFlippedX: boolean;
      isFlippedY: boolean;
      isKinematic: boolean;
      isPersistent: boolean;
      name: string;
      updateWhenPaused: boolean;
      alpha: number;
      ancestor: Container | Entity;
      anchorPoint: ObservableVector2d;
      currentTransform: Matrix2d;
      isDirty: boolean;
      mask: Rect | Polygon | Line | Ellipse;
      onVisibilityChange: (inViewport: boolean) => void;
      pos: ObservableVector3d;
      tint: Color;

      // TODO

      angleTo(target: Renderable | Vector2d | Vector3d): number;

      distanceTo(target: Renderable | Vector2d | Vector3d): number;

      onDestroyEvent(): void;

      draw(renderer: Renderer): void;

      flipX(flip?: boolean): this;

      flipY(flip?: boolean): this;

      getOpacity(): number;

      setOpacity(alpha: number): void;

      lookAt(target: Renderable | Vector2d | Vector3d): this;

      postDraw(renderer: Renderer): void;

      preDraw(renderer: Renderer): void;

      update(dt: number): boolean;
    }

    export class Renderer {
      constructor(options: Renderer.Options);
    }

    namespace Renderer {
      export interface Options {
        width: number;
        height: number;
        canvas?: HTMLCanvasElement;
        // TODO
      }
    }

    export class Sprite extends Renderable {
      constructor(x: number, y: number, settings: any);

      _super(cls: any, fn: string, args: readonly any[]): any;

      addAnimation(name: string, index: number[]): number;

      flicker(duration: number, callback: () => void): this;

      getCurrentAnimationFrame(): number;

      isCurrentAnimation(name: string): boolean;

      isFlickering(): boolean;

      reverseAnimation(name?: string): this;

      setAnimationFrame(index?: number): this;

      setCurrentAnimation(name: string, onComplete?: any): this;

      setRegion(region: any): this;

      static extend(options: Sprite.ExtendOptions): typeof Sprite;
    }

    namespace Sprite {
      export interface ExtendOptions {
        init(this: Sprite, x: number, y: number, settings: any): void;
      }
    }

    export class Stage {
      static extend(options: Stage.ExtendOptions): typeof Stage;
    }

    namespace Stage {
      export interface ExtendOptions {
        onResetEvent(): void;
      }
    }

    export class Vector2d {
      x: number;
      y: number;

      constructor(x?: number, y?: number);

      abs(): this;

      add(v: Vector2d): this;

      angle(v: Vector2d): number;

      ceil(v: Vector2d): Vector2d;

      ceilSelf(v: Vector3d): this;

      clamp(low: number, high: number): Vector2d;

      clampSelf(low: number, high: number): this;

      clone(): Vector2d;

      copy(v: Vector2d): this;

      distance(v: Vector2d): number;

      div(value: number): number;

      dotProduct(v: Vector2d): number;

      equals(v: Vector2d): boolean;

      floor(): Vector2d;

      floorSelf(): this;

      length(): number;

      length2(): number;

      lerp(v: Vector2d, alpha: number): this;

      maxV(v: Vector2d): this;

      minV(v: Vector2d): this;

      negate(): Vector2d;

      negateSelf(): this;

      normalize(): this;

      perp(): this;

      project(v: Vector2d): this;

      projectN(v: Vector2d): this;

      rotate(angle: number, v?: Vector2d): this;

      scale(x: number, y?: number): this;

      scaleV(v: Vector2d): this;

      set(x: number, y: number): this;

      setV(v: Vector2d): this;

      setZero(): this;

      sub(v: Vector2d): this;

      to2s(): this;

      toIso(): this;

      toString(): string;
    }

    export class TMXTileMap {
    }

    export class Vector3d {
      x: number;
      y: number;
      z: number;

      abs(): this;

      add(v: Vector2d): this;

      angle(v: Vector2d): number;

      ceil(v: Vector2d): Vector2d;

      ceilSelf(v: Vector3d): this;

      clamp(low: number, high: number): Vector2d;

      clampSelf(low: number, high: number): this;

      clone(): Vector2d;

      copy(v: Vector2d): this;

      distance(v: Vector2d): number;

      div(value: number): number;

      dotProduct(v: Vector2d): number;

      equals(v: Vector2d): boolean;

      floor(): Vector2d;

      floorSelf(): this;

      length(): number;

      length2(): number;

      lerp(v: Vector2d, alpha: number): this;

      maxV(v: Vector2d): this;

      minV(v: Vector2d): this;

      negate(): Vector2d;

      negateSelf(): this;

      normalize(): this;

      perp(): this;

      project(v: Vector2d): this;

      projectN(v: Vector2d): this;

      rotate(angle: number, v?: Vector2d): this;

      scale(x: number, y?: number): this;

      scaleV(v: Vector2d): this;

      set(x: number, y: number, z?: number): this;

      setV(v: Vector2d): this;

      setZero(): this;

      sub(v: Vector2d | Vector3d): this;

      to2s(): this;

      toIso(): this;

      toString(): string;
    }

    export class World extends Container {
      gravity: number;

      reset(): void;
    }
  }
}
